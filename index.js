const urlparser = url => {
  const urlFormat = "/:version/api/:collection/:id";
  const pathParams = {};
  let output = {};

  // Extract path variables by splitting
  if (url.includes("/")) {
    let arrUrl = url
      .substr(0, url.indexOf("?"))
      .split("/")
      .filter(el => el);

    // Split format url to and extract variable names if they include colons
    const arrFormat = urlFormat.split("/").filter(el => el);
    arrFormat.forEach((segment, index) => {
      if (segment.includes(":")) {
        // Extract words inside to properties
        const property = segment.match(/\w+/gm);
        pathParams[property] = parseNumber(arrUrl[index]);
      }
    });
  }
  // Extract url parameters
  const urlParams = {};
  if (url.includes("?")) {
    params = url.substring(url.indexOf("?") + 1, url.length).split("&");
    params.forEach(param => {
      const p = param.split("=");
      urlParams[p[0]] = parseNumber(p[1]);
    });
  }

  // Merge two objects
  output = { ...pathParams, ...urlParams };
  return output;
};

// Just a util function to parse to integer
const parseNumber = value => (!isNaN(value) ? Number.parseInt(value) : value);

module.exports = urlparser;
